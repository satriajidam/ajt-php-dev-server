#!/usr/bin/env bash

########################################################################
# Script  : Install PHP v7.1 & PHP-FPM.
# OSs     : - Ubuntu 16.04
# Authors : - Agastyo Satriaji Idam (play.satriajidam@gmail.com)
########################################################################

set -o errexit # make script exits when a command fails
set -o pipefail # make script exits when the rightmost command of a pipeline exits with non-zero status
set -o nounset # make script exits when it tries to use undeclared variables
# set -o xtrace # trace what gets executed for debugging purpose

# ensure running as root
if [ "$(id -u)" != "0" ]; then
  exec sudo "$0" "$@"
  exit $?
fi

begin_msg 'Installing PHP & PHP-FPM v7.1 ...'

apt-get install -y python-software-properties
add-apt-repository -y ppa:ondrej/php
apt-get update
apt-get install -y php7.1
apt-get install -y php7.1-fpm

success_msg 'PHP & PHP-FPM v7.1 installed!'

begin_msg 'Configuring PHP-FPM v7.1 ...'

systemctl enable php7.1-fpm
systemctl restart php7.1-fpm
systemctl status php7.1-fpm --no-pager

success_msg 'PHP-FPM v7.1 configured!'
