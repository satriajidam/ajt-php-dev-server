#!/usr/bin/env bash

########################################################################
# Script  : Remove all Docker containers, images, & volumes.
# OSs     : - Ubuntu 16.04
# Authors : - Agastyo Satriaji Idam (play.satriajidam@gmail.com)
########################################################################

set -o errexit # make script exits when a command fails
set -o pipefail # make script exits when the rightmost command of a pipeline exits with non-zero status
set -o nounset # make script exits when it tries to use undeclared variables
# set -o xtrace # trace what gets executed for debugging purpose

# ensure running as root
if [ "$(id -u)" != "0" ]; then
  exec sudo "$0" "$@"
  exit $?
fi

echo "Cleaning up docker..."

get_containers() {
  docker ps --all --quiet
}

get_images() {
  docker images --quiet
}

get_volumes() {
  docker volume ls --quiet
}

echo "Removing all containers..."
if [ "$(get_containers)" ]; then
  docker rm --volumes --force $(get_containers)
  echo "All containers successfully removed!"
else
  echo "No containers found!"
fi

echo "Removing all images..."
if [ "$(get_images)" ]; then
  docker rmi --force $(get_images)
  echo "All images successfully removed!"
else
  echo "No images found!"
fi

echo "Removing all volumes..."
if [ "$(get_volumes)" ]; then
  docker volume rm --force $(get_volumes)
  echo "All volumes successfully removed!"
else
  echo "No volumes found!"
fi

echo "Done!"
