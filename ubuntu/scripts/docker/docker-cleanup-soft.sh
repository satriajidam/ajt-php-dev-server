#!/usr/bin/env bash

########################################################################
# Script  : Remove Docker stopped containers, untagged images, & dangling volumes.
# OSs     : - Ubuntu 16.04
# Authors : - Agastyo Satriaji Idam (play.satriajidam@gmail.com)
########################################################################

set -o errexit # make script exits when a command fails
set -o pipefail # make script exits when the rightmost command of a pipeline exits with non-zero status
set -o nounset # make script exits when it tries to use undeclared variables
# set -o xtrace # trace what gets executed for debugging purpose

# ensure running as root
if [ "$(id -u)" != "0" ]; then
  exec sudo "$0" "$@"
  exit $?
fi

echo "Cleaning up docker..."

get_exited_containers() {
  docker ps --all --quiet --filter="status=exited"
}

get_dangling_images() {
  docker images --quiet --filter="dangling=true"
}

get_dangling_volumes() {
  docker volume ls --quiet --filter="dangling=true"
}

echo "Removing exited containers..."
if [ "$(get_exited_containers)" ]; then
  docker rm --volumes $(get_exited_containers)
  echo "Exited containers successfully removed!"
else
  echo "No exited containers!"
fi

echo "Removing untagged images..."
if [ "$(get_dangling_images)" ]; then
  docker rmi --force $(get_dangling_images)
  echo "Untagged images successfully removed!"
else
  echo "No untagged images!"
fi

echo "Removing dangling volumes..."
if [ "$(get_dangling_volumes)" ]; then
  docker volume rm $(get_dangling_volumes)
  echo "Dangling volumes successfully removed!"
else
  echo "No dangling volumes!"
fi

echo "Done!"
