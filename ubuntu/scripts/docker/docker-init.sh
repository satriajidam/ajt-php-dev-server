#!/usr/bin/env bash

########################################################################
# Script  : Docker configuration.
# OSs     : - Ubuntu 16.04
# Authors : - Agastyo Satriaji Idam (play.satriajidam@gmail.com)
########################################################################

set -o errexit # make script exits when a command fails
set -o pipefail # make script exits when the rightmost command of a pipeline exits with non-zero status
set -o nounset # make script exits when it tries to use undeclared variables
# set -o xtrace # trace what gets executed for debugging purpose

# ensure running as root
if [ "$(id -u)" != "0" ]; then
  exec sudo "$0" "$@"
  exit $?
fi

begin_msg 'Configuring Docker...'

# configure docker daemon
# ref: https://docs.docker.com/engine/reference/commandline/dockerd/#daemon-configuration-file
cat <<EOT > /etc/docker/daemon.json
{
  "storage-driver": "overlay2",
  "live-restore": true
}
EOT

print_content '/etc/docker/daemon.json'

# enable docker to start on boot
systemctl daemon-reload
systemctl enable docker
systemctl restart docker
systemctl status docker --no-pager

success_msg 'Docker configured!'
